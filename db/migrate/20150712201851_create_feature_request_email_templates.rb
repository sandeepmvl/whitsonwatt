class CreateFeatureRequestEmailTemplates < ActiveRecord::Migration
  def change
    create_table :feature_request_email_templates do |t|
      t.text :message

      t.timestamps
    end
  end
end
