class CreateReceiptScannerEmailTemplates < ActiveRecord::Migration
  def change
    create_table :receipt_scanner_email_templates do |t|
      t.text :message

      t.timestamps
    end
  end
end
