class CreateMeetingRequestToEmailTemplates < ActiveRecord::Migration
  def change
    create_table :meeting_request_to_email_templates do |t|
      t.text :message

      t.timestamps
    end
  end
end
