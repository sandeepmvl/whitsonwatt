class CreateMeetingRequesterEmailTemplates < ActiveRecord::Migration
  def change
    create_table :meeting_requester_email_templates do |t|
      t.text :message

      t.timestamps
    end
  end
end
