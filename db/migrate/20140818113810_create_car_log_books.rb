class CreateCarLogBooks < ActiveRecord::Migration
  def change
    create_table :car_log_books do |t|
      t.string :email
      t.string :registration
      t.string :distance
      t.date :log_start_date
      t.date :trip_date
      t.datetime :trip_start_time
      t.datetime :trip_end_time
      t.string :purpose
      t.text :description

      t.timestamps
    end
  end
end
