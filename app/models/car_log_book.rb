class CarLogBook < ActiveRecord::Base
  attr_accessible :email,:description, :log_start_date, :purpose, :registration, :trip_date, :trip_start_time, :trip_end_time, :distance
end