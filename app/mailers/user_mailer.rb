require 'erb'
require 'ostruct'

class UserMailer < ActionMailer::Base
  default :from => "wealth@whitsonwatt.com.au"

  def meeting_requester(invitation)
    @invitation = invitation
 	  @accountant_email = Accountant.find(invitation.accountant_id).email rescue "wealth@whitsonwatt.com.au"
		Time.zone = 'Brisbane' 
		time = invitation.appointment_time.to_s 
		@appointment_time = Time.zone.parse(time)
    @message = erb(MeetingRequesterEmailTemplate.last.message,invitation: @invitation,accountant_email: @accountant_email,appointment_time: @appointment_time)
    mail(:to => invitation.email, :subject => "Hello '#{@invitation.name}' you've scheduled a meeting with '#{@accountant_email}'")
  end

  def meeting_request_to(invitation)
    @invitation = invitation
 	  @accountant_email = Accountant.find(invitation.accountant_id).email rescue "wealth@whitsonwatt.com.au"
		Time.zone = 'Brisbane' 
		time = invitation.appointment_time.to_s 
		@appointment_time = Time.zone.parse(time)
    @message = erb(MeetingRequestToEmailTemplate.last.message,invitation: @invitation,accountant_email: @accountant_email,appointment_time: @appointment_time)
    mail(:to => @accountant_email, :subject => "You've been invited to join Meeting with '#{@invitation.name}'")
  end

  def send_feature_request_mail(feature_request)
    if Rails.env == "development"
      mail_to = 'railstest39@gmail.com'
    else
      mail_to = 'wealth@whitsonwatt.com.au'
    end 
    @suggester_name = feature_request.name
    @suggester_email = feature_request.email
    @suggester_suggestion = feature_request.suggestion
    @suggester_phone_number = feature_request.phone_number

    @message = erb(FeatureRequestEmailTemplate.last.message,suggester_name: @suggester_name, suggester_email: @suggester_email,suggester_suggestion: @suggester_suggestion, suggester_phone_number: @suggester_phone_number )

    mail(:to => mail_to, :subject => "A new suggestion from '#{@suggester_name}'")
  end

  def send_receipt_scanner_mail(receipt_scanner)
    if Rails.env == "development"
      mail_to = 'railstest39@gmail.com'
    else
      mail_to = receipt_scanner.email rescue 'wealth@whitsonwatt.com.au'
    end 
    @sender_email = receipt_scanner.email
    @receipt_date = receipt_scanner.date

    @message = erb(ReceiptScannerEmailTemplate.last.message,receipt_date: @receipt_date )

    mail(to: @sender_email,bcc: mail_to,subject: "Acknowledgement of receipt.")
  end

  private

  def erb(template, vars)
    ERB.new(template).result(OpenStruct.new(vars).instance_eval { binding })
  end

end