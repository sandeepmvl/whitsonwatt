module Api
  module V1
    class MeetingRequestToEmailTemplatesController < Api::ApiController
      # GET /meeting_request_to_email_templates
      # GET /meeting_request_to_email_templates.json
      def index
        @meeting_request_to_email_templates = MeetingRequestToEmailTemplate.all

        respond_to do |format|
          format.html # index.html.erb
          format.json { render json: @meeting_request_to_email_templates }
        end
      end

      # GET /meeting_request_to_email_templates/1
      # GET /meeting_request_to_email_templates/1.json
      def show
        @meeting_request_to_email_template = MeetingRequestToEmailTemplate.find(params[:id])

        respond_to do |format|
          format.html # show.html.erb
          format.json { render json: @meeting_request_to_email_template }
        end
      end

      # GET /meeting_request_to_email_templates/new
      # GET /meeting_request_to_email_templates/new.json
      def new
        @meeting_request_to_email_template = MeetingRequestToEmailTemplate.new

        respond_to do |format|
          format.html # new.html.erb
          format.json { render json: @meeting_request_to_email_template }
        end
      end

      # GET /meeting_request_to_email_templates/1/edit
      def edit
        @meeting_request_to_email_template = MeetingRequestToEmailTemplate.find(params[:id])
      end

      # POST /meeting_request_to_email_templates
      # POST /meeting_request_to_email_templates.json
      def create
        @meeting_request_to_email_template = MeetingRequestToEmailTemplate.new(params[:meeting_request_to_email_template])

        respond_to do |format|
          if @meeting_request_to_email_template.save
            format.html { redirect_to @meeting_request_to_email_template, notice: 'Meeting request to email template was successfully created.' }
            format.json { render json: @meeting_request_to_email_template, status: :created, location: @meeting_request_to_email_template }
          else
            format.html { render action: "new" }
            format.json { render json: @meeting_request_to_email_template.errors, status: :unprocessable_entity }
          end
        end
      end

      # PUT /meeting_request_to_email_templates/1
      # PUT /meeting_request_to_email_templates/1.json
      def update
        @meeting_request_to_email_template = MeetingRequestToEmailTemplate.find(params[:id])

        respond_to do |format|
          if @meeting_request_to_email_template.update_attributes(params[:meeting_request_to_email_template])
            format.html { redirect_to @meeting_request_to_email_template, notice: 'Meeting request to email template was successfully updated.' }
            format.json { head :no_content }
          else
            format.html { render action: "edit" }
            format.json { render json: @meeting_request_to_email_template.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /meeting_request_to_email_templates/1
      # DELETE /meeting_request_to_email_templates/1.json
      def destroy
        @meeting_request_to_email_template = MeetingRequestToEmailTemplate.find(params[:id])
        @meeting_request_to_email_template.destroy

        respond_to do |format|
          format.html { redirect_to meeting_request_to_email_templates_url }
          format.json { head :no_content }
        end
      end
    end
  end
end
