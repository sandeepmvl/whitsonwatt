#app/controllers/api/v1/news_feeds_controller.rb
module Api
	module V1
		class ReceiptScannerController < Api::ApiController
			class FilelessIO < StringIO
			  attr_accessor :original_filename
			end
			def create_receipt
				@receipt_scanner = ReceiptScanner.new
				@receipt_scanner.email = params[:email]
				@receipt_scanner.notes = params[:notes]
				@receipt_scanner.business_percentage = params[:business_percentage]
				@receipt_scanner.date = params[:date]
				@receipt_scanner.receipt_scanner_photo = params[:receipt_scanner_photo]
				@receipt_scanner.save!
    		UserMailer.send_receipt_scanner_mail(@receipt_scanner).deliver
			  render :json => MultiJson.dump(:status => "success")
			end
			def show
  			# @news_feeds = NewsFeed.find(params[:id])
			  # render :json => MultiJson.dump(:status => "sucess",:news_feeds => @news_feeds)
			end
		end
	end
end