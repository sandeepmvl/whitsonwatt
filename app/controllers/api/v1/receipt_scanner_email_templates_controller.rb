module Api
  module V1
    class ReceiptScannerEmailTemplatesController < Api::ApiController
      # GET /receipt_scanner_email_templates
      # GET /receipt_scanner_email_templates.json
      def index
        @receipt_scanner_email_templates = ReceiptScannerEmailTemplate.all

        respond_to do |format|
          format.html # index.html.erb
          format.json { render json: @receipt_scanner_email_templates }
        end
      end

      # GET /receipt_scanner_email_templates/1
      # GET /receipt_scanner_email_templates/1.json
      def show
        @receipt_scanner_email_template = ReceiptScannerEmailTemplate.find(params[:id])

        respond_to do |format|
          format.html # show.html.erb
          format.json { render json: @receipt_scanner_email_template }
        end
      end

      # GET /receipt_scanner_email_templates/new
      # GET /receipt_scanner_email_templates/new.json
      def new
        @receipt_scanner_email_template = ReceiptScannerEmailTemplate.new

        respond_to do |format|
          format.html # new.html.erb
          format.json { render json: @receipt_scanner_email_template }
        end
      end

      # GET /receipt_scanner_email_templates/1/edit
      def edit
        @receipt_scanner_email_template = ReceiptScannerEmailTemplate.find(params[:id])
      end

      # POST /receipt_scanner_email_templates
      # POST /receipt_scanner_email_templates.json
      def create
        @receipt_scanner_email_template = ReceiptScannerEmailTemplate.new(params[:receipt_scanner_email_template])

        respond_to do |format|
          if @receipt_scanner_email_template.save
            format.html { redirect_to @receipt_scanner_email_template, notice: 'Receipt scanner email template was successfully created.' }
            format.json { render json: @receipt_scanner_email_template, status: :created, location: @receipt_scanner_email_template }
          else
            format.html { render action: "new" }
            format.json { render json: @receipt_scanner_email_template.errors, status: :unprocessable_entity }
          end
        end
      end

      # PUT /receipt_scanner_email_templates/1
      # PUT /receipt_scanner_email_templates/1.json
      def update
        @receipt_scanner_email_template = ReceiptScannerEmailTemplate.find(params[:id])

        respond_to do |format|
          if @receipt_scanner_email_template.update_attributes(params[:receipt_scanner_email_template])
            format.html { redirect_to @receipt_scanner_email_template, notice: 'Receipt scanner email template was successfully updated.' }
            format.json { head :no_content }
          else
            format.html { render action: "edit" }
            format.json { render json: @receipt_scanner_email_template.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /receipt_scanner_email_templates/1
      # DELETE /receipt_scanner_email_templates/1.json
      def destroy
        @receipt_scanner_email_template = ReceiptScannerEmailTemplate.find(params[:id])
        @receipt_scanner_email_template.destroy

        respond_to do |format|
          format.html { redirect_to receipt_scanner_email_templates_url }
          format.json { head :no_content }
        end
      end
    end
  end
end
