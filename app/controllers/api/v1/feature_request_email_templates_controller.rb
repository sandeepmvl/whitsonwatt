module Api
  module V1
    class FeatureRequestEmailTemplatesController < Api::ApiController
      # GET /feature_request_email_templates
      # GET /feature_request_email_templates.json
      def index
        @feature_request_email_templates = FeatureRequestEmailTemplate.all

        respond_to do |format|
          format.html # index.html.erb
          format.json { render json: @feature_request_email_templates }
        end
      end

      # GET /feature_request_email_templates/1
      # GET /feature_request_email_templates/1.json
      def show
        @feature_request_email_template = FeatureRequestEmailTemplate.find(params[:id])

        respond_to do |format|
          format.html # show.html.erb
          format.json { render json: @feature_request_email_template }
        end
      end

      # GET /feature_request_email_templates/new
      # GET /feature_request_email_templates/new.json
      def new
        @feature_request_email_template = FeatureRequestEmailTemplate.new

        respond_to do |format|
          format.html # new.html.erb
          format.json { render json: @feature_request_email_template }
        end
      end

      # GET /feature_request_email_templates/1/edit
      def edit
        @feature_request_email_template = FeatureRequestEmailTemplate.find(params[:id])
      end

      # POST /feature_request_email_templates
      # POST /feature_request_email_templates.json
      def create
        @feature_request_email_template = FeatureRequestEmailTemplate.new(params[:feature_request_email_template])

        respond_to do |format|
          if @feature_request_email_template.save
            format.html { redirect_to @feature_request_email_template, notice: 'Feature request email template was successfully created.' }
            format.json { render json: @feature_request_email_template, status: :created, location: @feature_request_email_template }
          else
            format.html { render action: "new" }
            format.json { render json: @feature_request_email_template.errors, status: :unprocessable_entity }
          end
        end
      end

      # PUT /feature_request_email_templates/1
      # PUT /feature_request_email_templates/1.json
      def update
        @feature_request_email_template = FeatureRequestEmailTemplate.find(params[:id])

        respond_to do |format|
          if @feature_request_email_template.update_attributes(params[:feature_request_email_template])
            format.html { redirect_to @feature_request_email_template, notice: 'Feature request email template was successfully updated.' }
            format.json { head :no_content }
          else
            format.html { render action: "edit" }
            format.json { render json: @feature_request_email_template.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /feature_request_email_templates/1
      # DELETE /feature_request_email_templates/1.json
      def destroy
        @feature_request_email_template = FeatureRequestEmailTemplate.find(params[:id])
        @feature_request_email_template.destroy

        respond_to do |format|
          format.html { redirect_to feature_request_email_templates_url }
          format.json { head :no_content }
        end
      end
    end
  end
end
