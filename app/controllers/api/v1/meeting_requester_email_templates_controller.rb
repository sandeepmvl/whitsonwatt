module Api
  module V1
    class MeetingRequesterEmailTemplatesController < Api::ApiController
      # GET /meeting_requester_email_templates
      # GET /meeting_requester_email_templates.json
      def index
        @meeting_requester_email_templates = MeetingRequesterEmailTemplate.all

        respond_to do |format|
          format.html # index.html.erb
          format.json { render json: @meeting_requester_email_templates }
        end
      end

      # GET /meeting_requester_email_templates/1
      # GET /meeting_requester_email_templates/1.json
      def show
        @meeting_requester_email_template = MeetingRequesterEmailTemplate.find(params[:id])

        respond_to do |format|
          format.html # show.html.erb
          format.json { render json: @meeting_requester_email_template }
        end
      end

      # GET /meeting_requester_email_templates/new
      # GET /meeting_requester_email_templates/new.json
      def new
        @meeting_requester_email_template = MeetingRequesterEmailTemplate.new

        respond_to do |format|
          format.html # new.html.erb
          format.json { render json: @meeting_requester_email_template }
        end
      end

      # GET /meeting_requester_email_templates/1/edit
      def edit
        @meeting_requester_email_template = MeetingRequesterEmailTemplate.find(params[:id])
      end

      # POST /meeting_requester_email_templates
      # POST /meeting_requester_email_templates.json
      def create
        @meeting_requester_email_template = MeetingRequesterEmailTemplate.new(params[:meeting_requester_email_template])

        respond_to do |format|
          if @meeting_requester_email_template.save
            format.html { redirect_to @meeting_requester_email_template, notice: 'Meeting requester email template was successfully created.' }
            format.json { render json: @meeting_requester_email_template, status: :created, location: @meeting_requester_email_template }
          else
            format.html { render action: "new" }
            format.json { render json: @meeting_requester_email_template.errors, status: :unprocessable_entity }
          end
        end
      end

      # PUT /meeting_requester_email_templates/1
      # PUT /meeting_requester_email_templates/1.json
      def update
        @meeting_requester_email_template = MeetingRequesterEmailTemplate.find(params[:id])

        respond_to do |format|
          if @meeting_requester_email_template.update_attributes(params[:meeting_requester_email_template])
            format.html { redirect_to @meeting_requester_email_template, notice: 'Meeting requester email template was successfully updated.' }
            format.json { head :no_content }
          else
            format.html { render action: "edit" }
            format.json { render json: @meeting_requester_email_template.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /meeting_requester_email_templates/1
      # DELETE /meeting_requester_email_templates/1.json
      def destroy
        @meeting_requester_email_template = MeetingRequesterEmailTemplate.find(params[:id])
        @meeting_requester_email_template.destroy

        respond_to do |format|
          format.html { redirect_to meeting_requester_email_templates_url }
          format.json { head :no_content }
        end
      end
    end
  end
end
