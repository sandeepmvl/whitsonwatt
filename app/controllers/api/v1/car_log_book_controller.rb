#app/controllers/api/v1/car_log_book_controller.rb
module Api
	module V1
		class CarLogBookController < Api::ApiController
			def create_car_trip
				@car_trip = CarLogBook.new(params[:car_log])
				if @car_trip.save!
					render :json => MultiJson.dump(:status => "success",:car_trip_id => @car_trip.id)
				else
					render :json => MultiJson.dump(:status => "unprocessable_entity",:car_trip_create_errors => @car_trip.errors)
				end
			end
			def show_car_trip
				@car_trip = CarLogBook.find(params[:id])
   			render :json => {:status => "success", :car_trip_id => @car_trip}
				# render :json => MultiJson.dump(:status => "car trip details",:car_trip => @car_trip)
			end
			def update_car_trip
  			@car_trip = CarLogBook.find(params["car_log_update"]["car_trip_id"])
				@car_trip.update_attributes(:distance => params["car_log_update"]["distance"],:trip_end_time => params["car_log_update"]["trip_end_time"])
  			# @car_trip = CarLogBook.find(params[:id])
			  render :json => MultiJson.dump(:status => "sucess",:car_trip => @car_trip)
			end
		end
	end
end