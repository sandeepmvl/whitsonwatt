class ApplicationController < ActionController::Base
  # protect_from_forgery

  def my_erb_converter(template)
    erb = ERB.new(template)
    erb.result
  end

end